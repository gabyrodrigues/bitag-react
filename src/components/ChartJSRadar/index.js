import React from 'react';
import { Radar } from "react-chartjs-2";

const options = {
  scale: {
    ticks: { beginAtZero: true },
  },
}

const ChartJSRadar = (props) => (
  <>
    <Radar data={props.data} options={options} />
  </>
);

export default ChartJSRadar;
