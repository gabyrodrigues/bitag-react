import React, { useEffect, useState } from 'react';

import Header from '../../components/Header';
import { Col, Container, Row } from 'react-bootstrap';

import ChartJSLine from '../../components/ChartJSLine';
import ChartJSBar from '../../components/ChartJSBar';
import ChartJSRadar from '../../components/ChartJSRadar';

import axios from 'axios';

const GamesMultiComparison = () => {
  const [label, setLabel] = useState([]);
  const [price, setPrice] = useState([]);
  const [discount, setDiscount] = useState([]);

  useEffect(() => {
    axios.get("./steam.json")
    .then(result => {
      setLabel(Object.keys(result.data).map(key => result.data[key].release));
      setPrice(Object.keys(result.data).map(key => result.data[key].price));
      setDiscount(Object.keys(result.data).map(key => result.data[key].discount));
    });
  }, []);


  return (
    <div id="page-register">
      <Header title="BiTag" className="mb-5" />

      <Container>
        <h1 className="mb-5">Multi Jogo X vs Jogo Y</h1>
        <Row>
          <Col md={12}>
            <ChartJSLine
              data={{
                labels: label,
                datasets: [
                  {
                    label: "Releases price",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: price
                  },
                  {
                    label: "Releases discount",
                    backgroundColor: "rgba(111, 227, 255, 0.2)",
                    borderColor: "rgba(111, 227, 255, 1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(111, 227, 255, 0.4)",
                    hoverBorderColor: "rgba(111, 227, 255, 1)",
                    data: discount
                  }
                ]
              }}
            />
          </Col>
        </Row>

        <Row className="mt-5">
          <Col md={6}>
            <ChartJSBar
              data={{
                labels: label,
                datasets: [
                  {
                    label: "Releases price",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: price
                  },
                  {
                    label: "Releases discount",
                    backgroundColor: "rgba(111, 227, 255, 0.2)",
                    borderColor: "rgba(111, 227, 255, 1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(111, 227, 255, 0.4)",
                    hoverBorderColor: "rgba(111, 227, 255, 1)",
                    data: discount
                  }
                ]
              }}
            />
          </Col>

          <Col md={6}>
            <ChartJSRadar
              data={{
                labels: label,
                datasets: [
                  {
                    label: "Releases price",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: price
                  },
                  {
                    label: "Releases discount",
                    backgroundColor: "rgba(111, 227, 255, 0.2)",
                    borderColor: "rgba(111, 227, 255, 1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(111, 227, 255, 0.4)",
                    hoverBorderColor: "rgba(111, 227, 255, 1)",
                    data: discount
                  }
                ]
              }}
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default GamesMultiComparison;
