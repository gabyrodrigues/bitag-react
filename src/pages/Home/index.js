import React from 'react';
import Header from '../../components/Header';

import { Container, Row, Col } from 'react-bootstrap';
import LinkButton from '../../components/LinkButton';
import { FiPlus } from 'react-icons/fi';

import tech from '../../assets/tech.png';

const Home = () => {
  return (
    <div id="page-home">
      <Header title="BiTag" />
      <Container>
        <Row className="align-items-center">
          <Col>
            <h1>BiTag</h1>
            <p>Business Intelligence, Tags, Jogos.</p>

            <LinkButton className="btn-lg mt-3" to="/tags" variant="info">
              <FiPlus className="mr-2" />
              Acessar plataforma
            </LinkButton>
          </Col>
          <Col>
            <img src={tech} alt="Bitag" />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Home;
