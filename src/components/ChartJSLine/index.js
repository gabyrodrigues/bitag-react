import React from 'react';
import { Line } from "react-chartjs-2";

const options = {
  scales: {
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
      },
      {
        type: 'linear',
        display: true,
        position: 'right',
        id: 'y-axis-2',
        gridLines: {
          drawOnArea: false,
        },
      },
    ],
  },
}

const ChartJSLine = (props) => (
  <>
    <Line data={props.data} options={options} />
  </>
);

export default ChartJSLine;
