import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  body {
    background-color: #fff;
    color: var(--text-color);
    font-size: 16px;
    -webkit-font-smoothing: antialised;
  }

  html, body, #root {
    height: 100%;
  }

  a:hover {
    text-decoration: none !important;
  }

  body, input, button {
    font-family: Roboto, sans-serif;
  }

  button{
    cursor: pointer;
  }
`;
