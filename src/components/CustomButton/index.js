import React from 'react';

import { Button } from 'react-bootstrap';

const CustomButton = ({ className, children, type, variant, ...rest }) => {
	return (
		<Button type={type} className={`${className}`} variant={variant} {...rest}>
			{children}
		</Button>
	);
}

export default CustomButton;