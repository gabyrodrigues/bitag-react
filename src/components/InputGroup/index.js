import React from 'react';
import { Form } from 'react-bootstrap';

const InputGroup = ({ controlId, label, onChange, value, type, ...rest }) => {
  return (
    <Form.Group controlId="formBasicEmail">
      <Form.Label>{label}</Form.Label>
      <Form.Control
        type={type}
        value={value}
        onChange={onChange}
        {...rest}
      />
    </Form.Group>
  );
}

export default InputGroup;
