import React, { useEffect, useState } from 'react';

import Header from '../../components/Header';
import { Container, Row } from 'react-bootstrap';

import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  ComposedChart, Line, Area,
} from 'recharts';

import axios from 'axios';

const Tags = () => {
  const [games, setGames] = useState([]);
  const [steam, setSteam] = useState([]);

  useEffect(() => {
    axios.get('./games.json')
    .then((response)=>{
      setGames(response.data.games);
    }).catch((err)=>{
      console.log(err);
    });
  }, []);

  useEffect(() => {
    axios.get('./steam.json')
    .then((response)=>{
      setSteam(response.data);
    }).catch((err)=>{
      console.log(err);
    });
  }, []);

  return (
    <div id="page-register">
      <Header title="BiTag" className="mb-5" />

      <Container>
        <Row>
          <h1 className="mb-5">Jogo X</h1>
        </Row>

        <Row>
          <BarChart
            width={550}
            height={300}
            data={games}
            margin={{
              top: 5, right: 30, left: 20, bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="ano" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="quantidade" fill="#8884d8" />
            <Bar dataKey="teste" fill="#997541" />
          </BarChart>

          <BarChart
            width={550}
            height={300}
            data={steam}
            margin={{
              top: 5, right: 30, left: 20, bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="release" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="price" fill="#8884d8" unit="R$" />
          </BarChart>

          <ComposedChart
            width={550}
            height={300}
            data={games}
            margin={{
              top: 20, right: 20, bottom: 20, left: 20,
            }}
          >
            <CartesianGrid stroke="#f5f5f5" />
            <XAxis dataKey="ano" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Area type="monotone" dataKey="quantidade" fill="#8884d8" stroke="#8884d8" />
            <Bar dataKey="pv" barSize={20} fill="#413ea0" />
            <Line type="monotone" dataKey="quantidade" stroke="#ff7300" />
          </ComposedChart>

          <ComposedChart
            width={550}
            height={300}
            data={steam}
            margin={{
              top: 20, right: 20, bottom: 20, left: 20,
            }}
          >
            <CartesianGrid stroke="#f5f5f5" />
            <XAxis dataKey="release" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Area type="monotone" dataKey="owners" fill="#8884d8" stroke="#8884d8" />
            <Bar dataKey="price" barSize={20} fill="#413ea0" />
            <Line type="monotone" dataKey="discount" unit="%" stroke="#ff7300" />
          </ComposedChart>


        </Row>
      </Container>
    </div>
  );
}

export default Tags;
