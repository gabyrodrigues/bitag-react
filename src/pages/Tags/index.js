import React from 'react';

import { Container, Row } from 'react-bootstrap';
import Header from '../../components/Header';

import CustomButton from '../../components/CustomButton';
import LinkButton from '../../components/LinkButton';

const Tags = () => {
  return (
    <div id="page-register">
      <Header title="BiTag" className="mb-5" />

      <Container>
        <Row>
          <h1 className="mb-5">Tags</h1>
        </Row>

        <Row className="align-items-center justify-content-start">
          <LinkButton className="mr-2" to="/game" variant="primary">+ Ver jogo</LinkButton>
          <LinkButton className="mr-2" to="/gamescomparison" variant="primary">+ Comparar jogos</LinkButton>
          <LinkButton className="mr-2" to="/gamesmulticomparison" variant="primary">+ Comparar vários jogos</LinkButton>
          <CustomButton className="mr-2" variant="secondary">+ Ação</CustomButton>
          <CustomButton className="mr-2" variant="success">+ Casual</CustomButton>
          <CustomButton className="mr-2" variant="warning">+ Corridas</CustomButton>
          <CustomButton className="mr-2" variant="danger">+ Indie</CustomButton>
          <CustomButton className="mr-2" variant="info">+ Puzzle</CustomButton>
          <CustomButton className="mr-2" variant="light">+ RPG</CustomButton>
          <CustomButton className="mr-2" size="lg" variant="dark">+ Fantasia</CustomButton>
        </Row>
      </Container>
    </div>
  );
}

export default Tags;
