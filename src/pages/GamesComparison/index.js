import React, { useEffect, useState } from 'react';

import Header from '../../components/Header';
import { Col, Container, Row } from 'react-bootstrap';

import axios from 'axios';

import GraphicBar from '../../components/GraphicBar';
import GraphicStream from '../../components/GraphicStream';

const GamesComparison = () => {
  const [games, setGames] = useState([]);
  const [steam, setSteam] = useState([]);

  useEffect(() => {
    axios.get('./games.json')
    .then((response)=>{
      setGames(response.data.games);
    }).catch((err)=>{
      console.log(err);
    });
  }, []);

  useEffect(() => {
    axios.get('./steam.json')
    .then((response)=>{
      setSteam(response.data);
    }).catch((err)=>{
      console.log(err);
    });
  }, []);

  return (
    <div id="page-register">
      <Header title="BiTag" className="mb-5" />

      <Container>
        <h1 className="mb-5">Jogo X vs Jogo Y</h1>
        <Row>
          <Col md={12} style={{ height: 300 }}>
            <GraphicBar
              data={games}
              keys={['quantidade', 'pv', 'teste']}
              indexBy="ano"
              groupMode="grouped"
              axisBottom={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'Ano',
                  legendPosition: 'middle',
                  legendOffset: 32
              }}
              axisLeft={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'Quantidade',
                  legendPosition: 'middle',
                  legendOffset: -40
              }}
            />
          </Col>

          <Col md={12} style={{ height: 300 }}>
            <GraphicBar
              data={steam}
              keys={['price', 'discount']}
              indexBy="release"
              groupMode="stacked"
              axisBottom={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'Release date',
                  legendPosition: 'middle',
                  legendOffset: 32
              }}
              axisLeft={null}
            />
          </Col>

          <Col md={12} style={{ height: 500 }}>
            <GraphicStream
              data={games}
              keys={[ 'quantidade', 'pv', 'teste' ]}
              axisBottom={null}
              axisLeft={{ orient: 'left', tickSize: 5, tickPadding: 5, tickRotation: 0, legend: 'Quantidade', legendOffset: -50 }}
            />
          </Col>

          <Col md={12} style={{ height: 500 }}>
            <GraphicStream
              data={steam}
              keys={[ 'price', 'discount', 'owners' ]}
              axisBottom={null}
              axisLeft={{ orient: 'left', tickSize: 5, tickPadding: 5, tickRotation: 0, legend: 'Quantidade', legendOffset: -50 }}
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default GamesComparison;
