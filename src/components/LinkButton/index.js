import React from 'react';
import { Link } from 'react-router-dom';

const LinkButton = ({ className, variant, children, ...rest }) => {
  return (
    <Link className={`btn btn-${variant} ${className}`} variant={variant} {...rest}>
      {children}
    </Link>
  );
}

export default LinkButton;