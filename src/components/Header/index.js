import React from 'react';

import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { FiLogIn, FiPlus } from 'react-icons/fi';
import logo from '../../assets/logo.png';

const Header = (props) => {
  return (
    <>
      <Navbar collapseOnSelect className="mb-5" expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand>
            <Link to="/">
              <img className="d-inline-block align-top" src={logo} alt="BiTag logo" />
              <span className="ml-2 text-white">{props.title}</span>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <Link className="nav-link" to="/register">
                <FiLogIn className="mr-2" />
                Login
              </Link>
              <Nav.Link href="/register">
                <FiPlus className="mr-2" />
                Registrar
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Header;
