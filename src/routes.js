import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Register from './pages/Register';
import Tags from './pages/Tags';
import Game from './pages/Game';
import GamesComparison from './pages/GamesComparison';
import GamesMultiComparison from './pages/GamesMultiComparison';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/register" component={Register} />
        <Route path="/tags" component={Tags} />
        <Route path="/game" component={Game} />
        <Route path="/gamescomparison" component={GamesComparison} />
        <Route path="/gamesmulticomparison" component={GamesMultiComparison} />
      </Switch>
    </BrowserRouter>
  );
}
