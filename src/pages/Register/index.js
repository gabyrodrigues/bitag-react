import React, { useState } from 'react';

import { Container, Form, Row } from 'react-bootstrap';
import Header from '../../components/Header';
import InputGroup from '../../components/InputGroup';
import CustomButton from '../../components/CustomButton';

const Register = () => {
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');

  return (
    <div id="page-register">
      <Header title="BiTag" className="mb-5" />

      <Container>
        <Row>
          <h1 className="mb-5">Cadastro na plataforma </h1>

          <Form className="w-100">
            <InputGroup
              label="Email: "
              controlId="email"
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required="required"
            />

            <InputGroup
              label="Senha: "
              controlId="senha"
              type="senha"
              value={senha}
              onChange={e => setSenha(e.target.value)}
              required="required"
            />

            <CustomButton type="submit" variant="dark">
              Cadastrar
            </CustomButton>
          </Form>
        </Row>
      </Container>
    </div>
  );
}

export default Register;
